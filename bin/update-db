#!/usr/bin/python3
from __future__ import print_function

import os
import sys
import argparse

import setup_paths
import bugs
import debian_support
import security_db

parser = argparse.ArgumentParser(description="Update Security Tracker Sqlite database")
parser.add_argument('--verbose', '-v', action='store_true',
                    help="Enable verbose messages for DB update operations")
parser.add_argument('file', nargs='?', help='Database file to process')
args = parser.parse_args()

db_file = args.file

try:
    db = security_db.DB(db_file, verbose=args.verbose)
    new_file = False
except security_db.SchemaMismatch:
    os.unlink(db_file)
    db = security_db.DB(db_file, verbose=True)
    new_file = True
    
db.cursor().execute("PRAGMA synchronous = OFF")
cursor = db.writeTxn()

# Bug lists (CAN/CVE/DSA/DTSA)

try:
    warnings = db.readBugs(cursor, 'data')
except SyntaxError as e:
    if e.filename is None or e.lineno is None:
        print("error:", e)
    else:
        print("%s:%d: %s" % (e.filename, e.lineno, e.msg))
    sys.exit(1)
except debian_support.ParseError as e:
    e.printOut(sys.stderr)
    sys.exit(1)
except security_db.InsertError as e:
    for err in e.errors:
        print(err)
    sys.exit(1)
if warnings:
    for x in warnings:
        print(x)
    sys.exit(1)

# Packages

try:
    db.readPackages(cursor, 'data/packages')
except debian_support.ParseError as e:
    e.printOut(sys.stderr)
    sys.exit(1)

if new_file:
    db.commit(cursor)
    cursor = db.writeTxn()

# Calculate vulnerability information.

warnings = db.calculateVulnerabilities(cursor)
if warnings:
    for x in warnings:
        print(x)
    sys.exit(1)

# debsecan data

db.calculateDebsecan()

# Everything worked well.

db.commit(cursor)
